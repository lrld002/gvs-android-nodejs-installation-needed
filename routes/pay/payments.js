var express = require('express')
var router = express.Router();

var Openpay = require('openpay');
var constants = require('../../constants');

var openpay = new Openpay(constants.MERCHANT_ID, constants.PRIVATE_KEY);
openpay.setProductionReady(constants.ES_PRODUCCION);

router.put("/do", function(req, res, next){
    var sourceId = req.body.source_id;
    var amount = req.body.amount;
    var deviceSessionId = req.body.device_session_id;
    var customerId = req.body.customer_id;


    var chargeRequest = {
        'source_id': sourceId,
        'method': 'card',
        'amount': amount,
        'currency': 'MXN',
        'description': 'FARMACIAS GVS',
        'order_id': 'FGVS-NO-' + (new Date().getTime()),
        'device_session_id': deviceSessionId
    };

    openpay.customers.charges.create(customerId,chargeRequest, function(error, charge){
        if (error != null){
            res.status(error["http_code"]).send(error);
        } else {
            res.json(charge);
        }
    });
});

module.exports = router;
