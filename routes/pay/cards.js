var express = require('express')
var router = express.Router();

var Openpay = require('openpay');
var constants = require('../../constants');

var openpay = new Openpay(constants.MERCHANT_ID, constants.PRIVATE_KEY);
openpay.setProductionReady(constants.ES_PRODUCCION);

router.get('/:client', function(request, response, next){
    openpay.customers.cards.list(request.params.client, function(error, list){
        if (error != null){
            response.status(error["http_code"]).send(error);
        }
        else {
            response.send(list);
        }
    });
});

router.delete('/:user/:card', function (request, response, next){
    openpay.customers.cards.delete(request.params.user, request.params.card, function(error){
        if (error != null){
            response.status(error["http_code"]).send(error);
        }else {
            response.send("");
        }
    })
});

router.put('/new', function(req, res, next){
    var customerId = req.body.customer_id;
    var deviceSessionId = req.body.device_session_id;
    var tokenId = req.body.token_id;

    var cardRequest = {
        'token_id': tokenId,
        'device_session_id': deviceSessionId
    };

    openpay.customers.cards.create(customerId, cardRequest, function(error, card){
        if (error != null){
            res.status(error["http_code"]).send(error);
        }else {
            res.json(card);
        }
    });
});

module.exports = router;