var express = require('express')
var router = express.Router();

var Openpay = require('openpay');
var constants = require('../../constants');

var openpay = new Openpay(constants.MERCHANT_ID, constants.PRIVATE_KEY);
openpay.setProductionReady(constants.ES_PRODUCCION);

/*
    Se administran las funciones básicas de clientes tales como
    regresar la info no sensible de un cliente, crear un cliente 
    nuevo o eliminar un cliente

    ----------------------------------------------------------------------------------------------------------------------
    
    /new
    Crea un nuevo cliente con los parametros dados

    Se usa sólo información básica (Nombre y mail)

    Respuestas:

    Satisfactorio:
        {
            "id":"anbnldwgni1way3yp2dw",
            "name":"customer name",
            "last_name":null,
            "email":"customer_email@me.com",
            "phone_number":null,
            "address":null,
            "creation_date":"2014-05-20T16:47:47-05:00",
            "external_id":null,
            "store": {
                "reference": "OPENPAY02DQ35YOY7",
                "barcode_url": "https://sandbox-api.openpay.mx/barcode/OPENPAY02DQ35YOY7?width=1&height=45&text=false"
            },
            "clabe": "646180109400423323"
        }

    Erroneo:
        {
            "category" : "request",
            "description" : "The customer with id 'm4hqp35pswl02mmc567' does not exist",
            "http_code" : 404,
            "error_code" : 1005,
            "request_id" : "1981cdb8-19cb-4bad-8256-e95d58bc035c",
            "fraud_rules": [
                "Billing <> BIN Country for VISA/MC"
            ]
        }

    ----------------------------------------------------------------------------------------------------------------------
*/

router.get('/:id', function(req, res, next){
    openpay.customers.get(req.param.id, function(error, customer){
        if (error != null){
            res.status(error["http_code"]).send(error);
        }else {
            res.send(customer);
        }
    });
});

router.put('/new',function(req, res, next){
    var name = req.body.name;
    var email = req.body.email;
    var requires_account = req.body.requires_account;

    var customerRequest = {
        'name': name,
        'email': email,
        'requires_account': requires_account
    };
    openpay.customers.create(customerRequest, function(error, customer) {
        if (error != null){
            res.status(error["http_code"]).send(error);
        }else{
            res.send(customer);
        }
      });
});

router.delete('/:id', function(req, res, next){
    openpay.customers.delete(req.params.id, function(error){
        if (error != null){
            res.status(error["http_code"]).send(error);
        }else {
            var response = {
                'deleted' : req.params.id
            };
            res.json(response);
        }
    });
});

module.exports = router;